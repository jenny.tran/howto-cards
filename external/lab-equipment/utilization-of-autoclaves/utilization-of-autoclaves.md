---
layout: page
permalink: /external/lab-equipment/utilization-of-autoclaves/
shortcut: lab:utilization-of-autoclaves
redirect_from:
  - /cards/lab-equipment:utilization-of-autoclaves
  - /external/cards/lab-equipment:utilization-of-autoclaves
  - /lab/utilization-of-autoclaves
  - /external/external/lab-equipment/utilization-of-autoclaves/
  - /cards/lab:utilization-of-autoclaves
  - /external/cards/lab:utilization-of-autoclaves
---

# Utilization of autoclaves

Autoclaves are used to perform sterilization of liquids and instruments and decontamination of liquids, solid and waste that are contaminated with biohazard materials. For more general information about autoclaving systems, feel free to watch [this video](https://www.youtube.com/watch?v=Yfc1yjEuuhE) published by the World Health Organization (WHO).


Before using the autoclaves, check that the appliance is fine. Pay special attention to the integrity of the gasket. If you are aware of any damage or if an autoclave displays an error message, send [a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=07ac89b7db9ccb40b0b4f9851d9619ae&action_id=b03dc9f7db9ccb40b0b4f9851d96192) to the Instrument Care team.  At the end of the description, include “Please notify:” and add the names of the instrument responsible person as well as the responsible backup person.

-	[Safety rules](#safety-rules)
-   [Common issue](#common-issue)
-	[How to autoclave solids/instruments](#how-to-autoclave-solidsinstruments)
-	[How to autoclave liquids](#how-to-autoclave-liquids)
-	[How to autoclave wastes](#how-to-autoclave-wastes)
-	[How to check sterilization efficiency](#how-to-check-sterilization-efficiency)
-	[How to run a cycle in Systec autoclaves VX-150 and DX-65](#how-to-run-a-cycle-in-systec-autoclaves-vx-150-and-dx-65)
-	[How to run a cycle in SHP Laboklav autoclaves](#how-to-run-a-cycle-in-shp-laboklav-autoclaves) 
-	[Routine maintenance of autoclaves](#routine-maintenance-of-autoclaves)
-	[How to use Certoclav autoclave](#how-to-use-certoclav-autoclave-bt1-lcsb00509)


## Safety rules
1. 	Before using the autoclave for the first time, read the utilization instructions below
2. Always wear protective personal equipment: lab coat, thermos-resistant gloves and goggles
3. Risks associated with the utilization of the autoclave include:
    - Crushing or amputation of fingers or arm: if closing of the door is no longer damped by spring or hydraulics
    - Burns after sterilization: hot clouds of steam can escape and lead to burns and the surfaces inside the container can be hot after sterilization cycle
    - Heavy loads: fully-loaded baskets can be too heavy to lift as such, therefore the basket should be partially unloaded before lifting the basket
4. Each use of an autoclave should be recorded in the corresponding log book with the date, the cycle number and the initials of the operator

5. To avoid release of toxic smells in the lab: **NEVER AUTOCLAVE TOXIC OR VOLATILE CHEMICALS!**
Refer to the University Chemical and Biological Waste Concept to use the correct bin.
<div align="center">
<img src="img/img24.png" width="800px">
</div>

## Common issue

Flasks overfilled with agar can lead to over boiling and spill of agar within the autoclave. Would this happen please remove all items from the autoclave and place a sign “Do not use this instrument” on the instrument. Send [a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=07ac89b7db9ccb40b0b4f9851d9619ae&action_id=b03dc9f7db9ccb40b0b4f9851d96192b) to the Instrument Care team.
At the end of the description, include “Please notify:” and add the names of the instrument responsible person as well as the responsible backup person. Clean all inner surfaces of the chamber. Wait for the reply from the Instrument care team to run a program at 134°C (empty chamber) to remove the agar from the hoses and pipes:
- Systec: program 12
- Laboklav: program P6

## How to autoclave solids/instruments
To remember: steam must penetrate the load. Tolerance to temperature is linked to humidity. As an example the temperature of a sauna, which is dry heat is between 70 and 100°C. For hammam, where there is steam the temperature is between 40 and 50°C. Similar to humans, some bugs are able to survive to high dry heat. Therefore, you must allow steam to reach your load to ensure the sterilisation process. 
- Place the flexible temperature sensor in the holder provided for it on the lid

*Make sure to wrap the sensor so that the wire is coiled around the holder. If it is not, this could result in damage of the sensor or if the wire interferes with the closing of the lid, to a steam exhaust of the chamber during the run.*
<div align="center">
<img src="img/img1.png" width="150px">
</div>

- Load the autoclave with the products to be sterilized. Put the products in a **wire-mesh basket** . Do not use the bucket
<div align="center">
<img src="img/img2.png" width="350px">
</div>

-	If the instruments are autoclaved in containers, make sure that the lid is loose

<img src="img/img3.png" width="20px">
Do not autoclave liquids with the solids cycle, there is a risk of explosion due to building pressure inside the liquid container

<img src="img/img4.png" width="20px">
The door can be opened when the atmospheric pressure has been reached; therefore, the temperature of the appliance and the sterilized products can still be 100°C or more when the cycle is finished

-	Run a sterilization cycle (see [How to run a cycle in Systec autoclaves VX-150 and DX-65](#how-to-run-a-cycle-in-systec-autoclaves-vx-150-and-dx-65) or [How to run a cycle in Laboklav autoclaves](#how-to-run-a-cycle-in-shp-laboklav-autoclaves))
-	When the sterilization cycle is complete and the specific unloading temperature and pressure have been reached, the message “Cycle ended” or “Opening door” appears on the display
-	Press the “Open” display key and lift the door using the grip handles until it reaches the vertical stop
-	The autoclave chamber can now be unloaded. For instruments, carefully take them out and close the containers if applicable. **Be careful, items might still be hot.** You can use heat resistant gloves as PPE
<div align="center">
<img src="img/img25.png" width="200px">
</div>

-	If the autoclave is not further used for several hours, close the door and switch it **OFF**

##	How to autoclave liquids
-	Place the liquids to be autoclaved in a **wire-mesh basket**

*If you fear an over boiling or a leakage of one of your bottles, place the wire-mesh basket on top of the bucket. All liquids will accumulate in the bucket and will not influence your autoclaving process.*

⚠️Do not use the basket only perforated on the upper third:  the accumulation of condensation in the basket can lead to a lack of water and to the termination of the process or to a significant loss of volume

⚠️Always use temperature-resistant containers that can withstand temperatures > 121°C
-	Place the flexible temperature sensor in a reference container which has the same size and same fill volume as the largest container that contains the liquid to be sterilized. This is necessary to
    - Ensure that the wanted sterilization temperature is reached inside the liquid
    - Ensure a safe cooling temperature
<div align="center">

<img src="img/img6.png" width="400px">
</div>

-	The liquid can either be autoclaved with or without cap
    - Without: 5% of the volume can be lost due to boiling. Moreover, open containers should not be filled more than 75%
    - With: if the container is autoclaved with a lid, do not tightly close it. Simply slightly close the lid

<img src="img/img3.png" width="20px">Due to the pressure building in the container during the sterilization, a tightly closed container may be at risk for explosion

Never stack bottles on top of each other

Avoid using plastic bottles as containers, the heating time is longer

Avoid autoclaving bottles with high volume difference (i.e.: 50 mL and 1 L) to avoid liquid loss
-	Run a sterilization cycle (see [How to run a cycle in Systec autoclaves VX-150 and DX-65](#how-to-run-a-cycle-in-systec-autoclaves-vx-150-and-dx-65) or [How to run a cycle in Laboklav autoclaves](#how-to-run-a-cycle-in-shp-laboklav-autoclaves))
-	When the sterilization cycle is complete and the specific unloading temperature and pressure have been reached, the message “Cycle ended” or “Opening door” appears on the display
-	Press the “Open” display key and lift the door using the grip handles until it reaches the vertical stop
-	The autoclave chamber can now be unloaded. **Be careful, items might still be hot.** You can use heat resistant gloves as PPE
<div align="center">
<img src="img/img25.png" width="200px">
</div>

-	Close the lids when taking the containers out, liquid waste can now be discarded in the sink
-	If the autoclave is not further used for several hours, close the door and switch it **OFF**

##	How to autoclave wastes
The sorting of waste in our laboratories is explained in the [Waste Management Concept at LCSB](https://unisupport.docebosaas.com/learn/course/42/waste-management-concept), a mandatory e-learning for newcomers. This training describes each category of waste and how to treat them. In the category of **infectious waste, non contaminated by hazardous chemicals and / or toxins**, solid waste can be autoclaved as well as cell culture liquids. After the autoclaving cycle of these liquids (see [How to autoclave liquids](#how-to-autoclave-liquids)), they can be discarded into the sink.

For solid waste that can be autoclaved: 
-	Always autoclave the waste bags in biohazard waste bins to prevent accumulation of dirt in the autoclave that could damage the instrument
-	Do not close the autoclave bag, and open the red part of the lid of the biohazard bin for the autoclaving. Fix it open with autoclave tape
<div align="center">

<img src="img/img7.png" width="250px">
</div>

-	Place the flexible temperature sensor in the holder provided for it on the lid 
<div align="center">
<img src="img/img1.png" width="150px">
</div>

⚠️Only laboratory waste in solid form and with only a small proportion of liquid may be sterilized in the waste bags
-	Run a sterilization cycle (see [How to run a cycle in Systec autoclaves VX-150 and DX-65](#how-to-run-a-cycle-in-systec-autoclaves-vx-150-and-dx-65) or [How to run a cycle in Laboklav autoclaves](#how-to-run-a-cycle-in-shp-laboklav-autoclaves))
-	When the sterilization cycle is complete and the specific unloading temperature and pressure have been reached, the message “Cycle ended” or “Opening door” appears on the display
-	Press the “Open” display key and lift the door using the grip handles until it reaches the vertical stop
-	The autoclave chamber can now be unloaded. **Be careful, items might still be hot.** You can use heat resistant gloves as PPE
<div align="center">
<img src="img/img25.png" width="200px">
</div>

-	Close the waste bag, transfer it to household trash bag and store them in the dedicated boxes. Do not place more than one autoclave bag into a trash bag, otherwise the trash bag is too heavy for the cleaning staff
-	If the autoclave is not further used for several hours, close the door and switch it **OFF**

##	How to check sterilization efficiency
**For each cycle**, you should use autoclave tape on the items awaiting autoclaving. The exposure to steam during minimum 30 sec will induce a color change of the tape from white to dark brown stripes. 
This indicator allows distinguishing units that have been exposed to the sterilization cycle from those not unexposed. It ensures that the wastes are not evacuated from the lab before having undergone the decontamination cycle.
<div align="center">
<img src="img/img8.png" width="200px">
</div>

**Once a week**, a chemical indicator has to be placed in the autoclave with the load to ensure proper functioning (Comply Thermalog, 3M, 2134MM)
-	The indicator is placed in the first load of the week
-	For waste load, place the strip in a 50 mL Falcon tube, closed with aluminum foil without lid (it melts during the run) and tape it to the inside of the bin
<div align="center">
<img src="img/img9.png" width="250px">
</div>

-	For any other load, place the indicator in a beaker and close it with aluminum foil
-	Results: 
    -	Good: the blue reached the “safe” zone
    -	Bad: the blue did not reach the “safe” zone
<div align="center">
<img src="img/img10.png" width="700px">
</div>

-	Indicate the date on the strip and staple it to the log book 
-	If the blue band did not reach the “Safe” zone, please send [a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=07ac89b7db9ccb40b0b4f9851d9619ae&action_id=b03dc9f7db9ccb40b0b4f9851d96192b) to the Instrument Care team with LCSB number and the error message

##	How to run a cycle in Systec autoclaves VX-150 and DX-65
<div align="center">
<img src="img/img11.png" width="300px">
</div>

-	Fill the autoclave depending on the nature of the product (described previously)
-	Close the door by pressing down with both hands until the seal comes in contact with the chamber and a vacuum is generated. You hear a sound signal and the device displays “Ready” as the door is successfully locked
-	 Press on “Menu” key on the display, go to “Select cycle” and press “Set”
-	Select the right program cycle using the arrows and press “Set”
<div align="center">
<img src="img/img12.png" width="600px">
</div>

-	Press “Start” and fill the log book with the date, the cycle number and your initials
-	In case of an incomplete run, please send [a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=07ac89b7db9ccb40b0b4f9851d9619ae&action_id=b03dc9f7db9ccb40b0b4f9851d96192b) to the Instrument Care team with LCSB number and the error message.
**In such case, the lid can’t be open due to temperature security and the Instrument Care team must get involved.**
The uncompleted cycle should be repeated after fixing of the issue with the autoclave and or by using another autoclave.

##	How to run a cycle in SHP Laboklav autoclaves
<div align="center">
<img src="img/img13.png" width="200px">
</div> 

-	Fill the autoclave depending on the nature of the product (described previously)
-	Press the door down until the seal comes in contact with it
-	Press on the “down arrow” while holding the door until the message “Door closing” disappears and the message “Ready” on the display confirm that the door has successfully been locked
-	If you did not succeed, open back the door by pressing the “up arrow”, then close it again by pressing on the “down arrow” 
-	Press on the “Program” key on the display and select the cycle using the arrows up and down and press “Enter” to validate your choice
<div align="center">
<img src="img/img14.png" width="600px">
</div>

-	Press “Start” and fill the log book with the date, the cycle number and your initials
-	In case of an incomplete run, please send [a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=07ac89b7db9ccb40b0b4f9851d9619ae&action_id=b03dc9f7db9ccb40b0b4f9851d96192b) to the Instrument Care team with LCSB number and the error message.
**In such case, the lid can’t be open due to temperature security and the Instrument Care team must get involved.**
The uncompleted cycle should be repeated after fixing of the issue with the autoclave and or by using another autoclave.

##	Routine maintenance of autoclaves
### 	**1. Systec autoclaves**

**Every month** or in case of spill, the autoclaves should be maintained:
1. Clean the inside of the chamber and the baskets with a mild cleaning reagent and a soft cloth or sponge
2. Clean the inside of the lid with a mild cleaning reagent and a soft cloth or sponge
3. Wipe the gasket
4. Clean the dirt strainer: it can be taken out without tools, rinse all dirt and place it back
<div align="center">
Systec VX-150: the dirt strainer is located in the middle of the floor

<img src="img/img15.png" width="100px">

Systec DX-65: the dirt strainer is located in the back of the floor

<img src="img/img16.png" width="100px">
</div>

5. Indicate the maintenance done in the logbook

**Every 150 cycles** or so, the exhaust filter cartridge should be replace.
This step is performed by the Instrument Care team. If the message appears on the display, send [a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=07ac89b7db9ccb40b0b4f9851d9619ae&action_id=c204728ddb12c7000823f3561d961910). 
At the end of the description section, include “Please notify:” and add the names of the instrument responsible person as well as the responsible backup person.

**Every year**,  Instrument Care Team organizes a check carried out by manufacturer and inspection body.


### **2. SHP Laboklav autoclaves**

**Every month** or in case of spill, the autoclaves should be maintained:
1. Clean the inside of the chamber and the baskets with a mild cleaning reagent and a soft cloth or sponge
2. Clean the inside of the lid with a mild cleaning reagent and a soft cloth or sponge
3. Wipe the gasket
4. Indicate the maintenance done in the logbook

**Every 500 cycles** or so, the exhaust filter cartridge should be replaced
This step is performed by the Instrument Care team. If the message appears on the display, send [a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=07ac89b7db9ccb40b0b4f9851d9619ae&action_id=c204728ddb12c7000823f3561d961910). 
At the end of the description section, include “Please notify:” and add the names of the instrument responsible person as well as the responsible backup person.

**Every year**,  Instrument Care Team organizes a check carried out by manufacturer and inspection body.

##	How to use Certoclav autoclave (BT1, LCSB00509)
This instrument is designed for sterilization of liquids.
<div align="center">
<img src="img/img17.png" width="500px">

**Never operate the unit without water**
</div>
<img src="img/img3.png" width="20px">
Never sterilize tightly sealed bottles

<img src="img/img4.png" width="20px">
The casing of the appliance can reach temperature above 80°C. Use the handles and gloves when manipulating the appliance

<img src="img/img4.png" width="20px">
Do not allow the steam to escape in the direction of persons

<img src="img/img3.png" width="20px">
Do not operate the appliance in room where there exists a risk of explosion

-	Add osmotic water to the indicated level (1-1.5L). *Do not use MilliQ water, it will cause corrosion of the appliance*
-	Add the material to sterilize to the basket of the Certoclav. Open the lids of the bottles slightly
-	Make sure the orange switch on the handles is turned towards <img src="img/img18.png" width="30px">
-	Line up the arrows on the lid and the main body of the unit
<div align="center">
<img src="img/img19.png" width="250px">
</div>

-	Close the lid lightly by turning clockwise and move the orange switch to lock   <img src="img/img20.png" width="30px">
-	Turn the maximum indicator of the manometer to “0” (use the black screw to bring the red arrow to 0)
<div align="center">
<img src="img/img21.png" width="250px">
</div>

-	Set the pressure control valves to 125°C/115°C by turning anticlockwise but don’t open the small screw
<div align="center">
<img src="img/img22.png" width="250px">
</div>

-	Set the thermostat knob to 125°C
-	Keep the steam release cock open (turn anticlockwise) 
<div align="center">
<img src="img/img23.png" width="250px">
</div>

-	Turn the Certoclav on via the green main switch
-	After 10 minutes, close the steam release cock (turn clockwise)
-	Upon closing of the steam release cock, it takes approximately 3 minutes for the pressure to rise and bring the unit to working temperature
-	For a complete sterilization, allow the unit to run for 20 more minutes.
The unit does NOT automatically stop the cycle, therefore, **PLEASE SET A TIMER** as extended heating can damage the unit
-	Switch the unit off using the green main switch
-	Check that the manometer is displaying “0”
-	Allow the temperature to drop to 70°C (approximately 40 minutes, the temperature can be seen on the thermometer)
-	When the cycle is done, open the steam release cock (turn anticlockwise)
-	Turn the orange knob on the handle to position <img src="img/img18.png" width="30px">
-	Turn the lid anticlockwise to remove it
-	For each cycle, fill the log book with the date, the cycle number and your initials

**Every 2 months**, the appliance should be cleaned:
1. Plug off the appliance and make sure that it has cooled down
2. Wipe the inside and outside of the appliance with a damp cloth. **NEVER immerse the lid or the chamber of the unit in water**

**Every 6 months**, the appliance should be disinfected:
1. Plug off the appliance and make sure that it has cooled down
2. Wipe the inside and outside of the appliance with 70% ethanol

**Every year**,  Instrument Care Team organizes a check carried out by manufacturer and inspection body.